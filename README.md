# using_docker

POC for containers

* Project structure:

```bash

identidock/
├── Dockerfile
└── app
    └── identidock.py

```

- Build image from `identidock` context: `docker build -t <USERNAME>/<IMAGE_NAME>:tag_name .`  
- Start container and bind-mount the source code folder: `docker run -d -p 5000:500 -v "$PWD"/app:/app <IMAGE_NAME>` and `sed -i '' s/World/Docker app/identidock.py` to see the changes
- Start http server on 9090 and stats server on 9191: `docker run -p 9090:9090 -p 9191:9191 <IMAGE_NAME>`
